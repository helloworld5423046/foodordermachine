import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodOrderMachine {
    private JButton hamburgerButton;
    private JButton cheeseburgerButton;
    private JButton teriyakiburgerButton;
    private JButton eggcheeseburgerButton;
    private JButton spicyburgerButton;
    private JButton fishburgerButton;
    private JPanel root;
    private JButton CheckOut;
    private JTextPane textPane;
    private JLabel total;

    double price;

    int totalPrice = 0;


    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodOrderMachine");
        frame.setContentPane(new FoodOrderMachine().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }



    void order(String foodName,int foodPrice) {

        int comfirmation = JOptionPane.showConfirmDialog(null,
                "World you like to order " + foodName + "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);


        if(comfirmation == 0){

            int place = JOptionPane.showConfirmDialog(null,
                    "Do you eat in the store?",
                    "Place Confirmation",
                    JOptionPane.YES_NO_OPTION);

            if(place==0) { //take in

                price = foodPrice * 1.1;
                totalPrice += price;

                String currentText = textPane.getText();
                textPane.setText(currentText + foodName + " " + (int) price + "yen\n");


                JOptionPane.showMessageDialog(null, "Order for " + foodName + "! It will served as soon as possible.");


                total.setText(totalPrice + " yen");
            }

            if(place==1) {//take out

                price = foodPrice * 1.08;
                totalPrice += price;

                String currentText = textPane.getText();
                textPane.setText(currentText + foodName + " " + (int) price + "yen\n");


                JOptionPane.showMessageDialog(null, "Order for " + foodName + "! It will served as soon as possible.");


                total.setText(totalPrice + " yen");
            }
        }

    }

    public FoodOrderMachine() {
        hamburgerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                order("Hamburger",170);

            }
        });
        cheeseburgerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                order("Cheeseburger",200);

            }
        });
        teriyakiburgerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                order("Teriyakiburger",400);

            }
        });
        eggcheeseburgerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                order("Eggcheeseburger",240);

            }
        });
        spicyburgerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                order("Spicyburger",220);

            }
        });
        fishburgerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                order("Fishburger",370);

            }
        });


        CheckOut.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                int comfirmation = JOptionPane.showConfirmDialog(null,
                        "World you like to checkout?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION);

                if(comfirmation == 0){

                JOptionPane.showMessageDialog(null, "Thank you. The total price is "+ totalPrice + " yen.");
                totalPrice=0;
                total.setText(totalPrice + " yen");

                String currentText=null;
                textPane.setText(currentText);

             }

            }
        });
    }
}
